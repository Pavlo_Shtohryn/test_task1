package com.shtohryn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchTest {
    @Test
    public void googleSearch() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        String title = "Apple";
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.google.com.ua/");
        WebElement webElement = webDriver.findElement(By.name("q"));
        webElement.sendKeys(title);
        webElement.submit();
        String verifyWord = "apple";
        new WebDriverWait(webDriver, 20).until(d -> d.getTitle()
                .toLowerCase().startsWith(verifyWord.toLowerCase()));
        String title1 = webDriver.getTitle();
       Assertions.assertTrue(!title1.concat(title).equalsIgnoreCase(verifyWord));
       System.out.print("Title is correct");
    }
}
